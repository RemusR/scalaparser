package vodafone

object Main {

  def main(args: Array[String]): Unit = {

    println("/* ********************* Starting CSV Converter ************************************* */")
    Converter.convertCSVFile()
    println("/* --------------------- Ending CSV Converter --------------------------------------- */\n\n\n")
    /* ****************************************************************************** */

  }

}
