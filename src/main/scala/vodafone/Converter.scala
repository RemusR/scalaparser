package vodafone

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction

import scala.util.matching.UnanchoredRegex

object Converter {

  //Direct private field access
  private[this] val numberRegex: UnanchoredRegex = "(\\d+)".r.unanchored

  import org.apache.spark.sql.functions.udf

  private val _extractAntenna: UserDefinedFunction = udf { s: String =>
    if (s.contains("_O_")) {
      numberRegex.findAllIn(s).toList(1)
    } else
      numberRegex.findAllIn(s).toList.head
  }

  def convertCSVFile() = {

    val spark = SparkSession
      .builder()
      .appName("CSV Parser")
      .master("local")
      .getOrCreate()

    import spark.sqlContext.implicits._
    import org.apache.spark.sql.functions.lit

    spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true") // Use first line of all files as header
      .option("inferSchema", "true") // Automatically infer data types
      .option("delimiter", ";")
      .load(Config.inputPath)
      .withColumn("site_id", _extractAntenna($"site"))
      .withColumn("OPCO", lit("RO"))
      .withColumn("EQUIP_CODE", lit("NaN"))
      .select(
        $"start" alias "PLANNED_HOUR",
        $"site_id" alias "SITE",
        $"date" alias "PLANNED_DATE",
        $"description" alias "WORK_TYPE")
      .write
      .csv(Config.outputPath)

    spark.stop()
  }

}
